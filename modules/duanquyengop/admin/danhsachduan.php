<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}

include 'project.php';
$page_title = $lang_module['danhsachduan'];

$sql = "SELECT * FROM `nv4_vi_duanquyengop_themmoiduans` WHERE `is_duyet` != 2";
$list = $db->query($sql)->fetchAll();

if($nv_Request->isset_request('change','post,get')){
    $change = check($_POST['id'],'id',$table_duan);
    $_POST['is_open'] = $change['is_open'] == 1 ? 2 : 1;
    $update = update($_POST['id'], $_POST , $table_duan);
    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
    
}

if($nv_Request->isset_request('unduyet','post,get')){
    $_POST['is_duyet'] = 3;
    $update = update($_POST['id'], $_POST , $table_duan);
    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
    
}

if($nv_Request->isset_request('update','post,get')){
    $change = check($_POST['id'],'id',$table_duan);
    if($change){
        nv_jsonOutput([
            'data' => $change,
        ]);
    }
    
}

if($nv_Request->isset_request('delete','post,get')){
    $sql = 'DELETE FROM `nv4_vi_duanquyengop_themmoiduans` WHERE id='. $_POST['id'];
    $kq  = $db->query($sql);
    if($kq){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
    
}

if($nv_Request->isset_request('accept_update','post,get')){
    $err = request($_POST,$array_name);
    // print_r($err);die;
    if($err){
        nv_jsonOutput([
            'status' => false,
            'err'   => $err,
        ]);
    }else{
        $list_anh = '';
        $link_luu_anh = NV_ROOTDIR . '/' . NV_UPLOADS_DIR . '/' . $module_name;
        if($_POST['so_luong_anh'] > 0){
            for ($i=0; $i < $_POST['so_luong_anh']; $i++) { 
                $upload = new NukeViet\Files\Upload('images', $global_config['forbid_extensions'], $global_config['forbid_mimes'], NV_UPLOAD_MAX_FILESIZE, 1600, 300);
                $upload->setLanguage($lang_global);
                $upload_info = $upload->save_file($_FILES['hinh_anh_' . $i] , $link_luu_anh, false, $global_config['nv_auto_resize']);
                if (strlen($list_anh) > 10) {
                    $list_anh .= ',' . NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
                } else {
                    $list_anh = NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
                }
            }
            $_POST['hinh_anh'] = $list_anh;
        }else{
            $_POST['hinh_anh'] = $_POST['link_image'];
        }
        foreach($fillable_duan as $value){
            $post[$value] = $_POST[$value];
        }
        $capnhat = update($_POST['id'], $post, $table_duan);
        if($capnhat){
            nv_jsonOutput([
                'status' => true,
            ]);
        }
    }    
}

if($nv_Request->isset_request('khongduyet','post,get')){
    $_POST['is_duyet'] = 2;
    $update = update($_POST['id'], $_POST , $table_duan);
    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
    
}


if($nv_Request->isset_request('duyetduan','post,get')){

    if(empty($_POST['ma_du_an'])){
        nv_jsonOutput([
            'status' => false,
            'mess' => 'Mã dự án không được để trống !',
        ]);
    }
    $check = check($_POST['id'],'id',$table_duan);
    if($check['ma_du_an'] == $_POST['ma_du_an']){
        nv_jsonOutput([
            'status' => false,
            'mess' => 'Mã dự án đã tồn tại !',
        ]);
    }else{
        $_POST['is_duyet'] = 1;
        $update = update($_POST['id'], $_POST, $table_duan);
    }


    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
}
$xtpl = new XTemplate('danhsachduan.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
foreach($list as $key => $value){
    if($value['is_open'] == 1){
        $x = '<button class="change btn btn-primary" data-id="'. $value['id'] .'">Hiển thị</button>';
        $xtpl->assign('IS_OPEN',$x);
    }else{
        $x = '<button class="change btn btn-danger" data-id="'. $value['id'] .'">Tạm tắt</button>';
        $xtpl->assign('IS_OPEN',$x); 
    }

    if($value['is_duyet'] == 3){
        $x = '<button class="duyet btn btn-primary" data-id="'. $value['id'] .'" data-toggle="modal" data-target="#duyetModal">Duyệt</button>
              <button class="khongduyet btn btn-warning" data-id="'. $value['id'] .'">Không duyệt</button>';
        $xtpl->assign('IS_DUYET',$x); 
    }else{
        $x = '<button class="unduyet btn btn-success" data-id="'. $value['id'] .'">Đã duyệt</button>'; 
        $xtpl->assign('IS_DUYET',$x); 
    }

    $value['hinh_anh'] = explode(',' , $value['hinh_anh'])[0];
    $xtpl->assign('VALUE',$value);
    $xtpl->assign('KEY',$key + 1);
    $xtpl->parse('main.list_du_an');
}

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
