<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

$table_duan = "nv4_vi_duanquyengop_themmoiduans";
$fillable_duan = [
    'ten_du_an',
    'mo_ta_ngan',
    'mo_ta_chi_tiet',
    'hinh_anh',
    'thoi_han',
    'so_tien',
    'is_open',
    'slug_du_an',
];

$array_name = [
    'ten_du_an'         => 'Tên Dự Án',
    'so_tien'           => 'Tiền Quyên Góp',
    'hinh_anh'          => 'Hình Ảnh Dự Án',
    'mo_ta_chi_tiet'    => 'Mô Tả Chi Tiết',
    'thoi_han'          => 'Thời hạn',
    'mo_ta_ngan'        => 'Mô Tả Ngắn',
    'is_open'           => 'Tình Trạng',
];

$array_name_update = [
    'id'                => 'Id không được để trống',
    'ten_du_an'         => 'Tên Dự Án',
    'so_tien'           => 'Tiền Quyên Góp',
    'hinh_anh'          => 'Hình Ảnh Dự Án',
    'mo_ta_chi_tiet'    => 'Mô Tả Chi Tiết',
    'thoi_han'          => 'Thời hạn',
    'mo_ta_ngan'        => 'Mô Tả Ngắn',
    'is_open'           => 'Tình Trạng',
];
