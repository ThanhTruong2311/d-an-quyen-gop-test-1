<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_MOD_PAGE')) {
    exit('Stop!!!');
}
include 'project.php';
$table_lich_su = 'nv4_vi_duanquyengop_lich_su_quyen_gops';
global $user_info;
$id = $nv_Request->get_int('id','post,get');
$sql = 'SELECT * FROM `nv4_vi_duanquyengop_themmoiduans` WHERE id=' . $id;
$data = $db->query($sql)->fetch();
$hinh_anh = explode(',' ,$data['hinh_anh']);

if($nv_Request->isset_request('payment', 'post,get')){
    $_POST['id_nguoi_quyen_gop'] = $user_info['userid'];
    $_POST['ngay_quyen_gop'] = date('Y/m/d');
    $store = store_client($_POST, $table_lich_su);
    $username = substr($_POST['email_quyen_gop'], 0 , strpos($_POST['email_quyen_gop'] , '@'));
    $md5username = nv_md5safe($username);
    if($store){
        $post = [
            'username' => $username,
            'group_id'  => 4,
            'md5username'  => $md5username,
            'password'  => $crypt->hash_password("123123", $global_config['hashprefix']),
            'email'  => $_POST['email_quyen_gop'],
            'email_verification_time' => -1,
            'active' => 1,
            'birthday' => NV_CURRENTTIME,
            'question' => 'Done',
        ];
        
        $check_email = check_client($post['email'],'email', 'nv4_users');

        if(empty($check_email)){
            $store_user = store_client($post,'nv4_users');
            if($store_user){
                $from = [];
                $message = 'Cảm ơn bạn đã ủng hộ';
                nv_sendmail($from,$post['email'],'Thư cảm ơn người ủng hộ',$message);
                
                nv_jsonOutput([
                    'status' =>true,
                ]);
            }
        }

        
    }
}

$xtpl = new XTemplate('chitietduan.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
$xtpl->assign('V', $data);
foreach($hinh_anh as $key => $value){
    if($key == 0){
        $x = 'item active';
        $xtpl->assign('ACTIVE', $x);
    }else{
        $x = 'item';
        $xtpl->assign('ACTIVE', $x);
    }
    $xtpl->assign('HINH_ANH', $value);
    $xtpl->parse('main.hinh');
};
$xtpl->parse('main');
$contents = $xtpl->text('main');
include NV_ROOTDIR . '/includes/header.php';
echo nv_site_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
