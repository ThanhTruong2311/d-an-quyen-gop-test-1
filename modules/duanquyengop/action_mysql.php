<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_MODULES')) {
    exit('Stop!!!');
}

$sql_drop_module = [];

$sql_drop_module[] = 'DROP TABLE IF EXISTS ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . ';';
$sql_drop_module[] = 'DROP TABLE IF EXISTS ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . '_config;';
$sql_drop_module[] = 'DROP TABLE IF EXISTS ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . '_themmoiduans;';
$sql_drop_module[] = 'DROP TABLE IF EXISTS ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . '_lich_su_quyen_gops;';
$sql_drop_module[] = 'DROP TABLE IF EXISTS ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . '_card_client;';

$sql_create_module = $sql_drop_module;

$sql_create_module[] = 'CREATE TABLE ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . '_themmoiduans (
    id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
    ma_du_an varchar(250) NULL,
    ten_du_an varchar(250) NOT NULL,
    slug_du_an varchar(250) NOT NULL,
    mo_ta_ngan text NOT NULL,
    mo_ta_chi_tiet text NOT NULL,
    hinh_anh varchar(250) NOT NULL,
    thoi_han date NOT NULL,
    so_tien int(11) NOT NULL,
    is_open int(11) NOT NULL,
    is_duyet int(11) default 3,
    id_nguoi_tao int(11) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    UNIQUE KEY ma_du_an (ma_du_an),
    UNIQUE KEY slug_du_an (slug_du_an)
   )ENGINE=MyISAM';

   $sql_create_module[] = 'CREATE TABLE ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . '_lich_su_quyen_gops (
    id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
    transaction_id varchar(250) NOT NULL,
    full_name varchar(250) NOT NULL,
    ma_du_an varchar(250) NOT NULL,
    id_du_an int(11) NOT NULL,
    id_nguoi_quyen_gop int(11) NOT NULL,
    so_tien_quyen_gop double NOT NULL,
    ngay_quyen_gop date NOT NULL,
    email_quyen_gop varchar(250) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
   )ENGINE=MyISAM';
   
   $sql_create_module[] = 'CREATE TABLE ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . '_card_client (
    id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
    user_id int(11) NOT NULL,
    slogan varchar(250) NOT NULL,
    facebook varchar(250) NOT NULL,
    instagram varchar(250) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
   )ENGINE=MyISAM'; 

//    $sql_create_module[] = 'CREATE TABLE ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . "_card_client(
//     id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
//     user_id int(11) NOT NULL,
//     slogan varchar(250) NULL,
//     facebook varchar(250) NULL,
//     instagram varchar(255) NULL,
//     zalo varchar(255) NULL,
//     PRIMARY KEY (id),
//     ) ENGINE=MyISAM";


   
