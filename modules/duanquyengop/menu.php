<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}

$row = [
    [
        'id' => 1,
        'title' => 'Tạo Dự án',
        'alias' => 'taoduan',
    ],

    [
        'id' => 2,
        'title' => 'Card profile',
        'alias' => 'card_profile',
    ],

    [
        'id' => 3,
        'title' => 'Card',
        'alias' => 'card',
    ],

    [
        'id' => 4,
        'title' => 'Lịch sử quyên góp',
        'alias' => 'lichsuquyengop',
    ],
];

foreach($row as $key => $value) {
    $array_item[$value['id']] = [
        'key' => $value['id'],
        'title' => $value['title'],
        'alias' => $value['alias'] . $global_config['rewrite_exturl']
    ];
}
